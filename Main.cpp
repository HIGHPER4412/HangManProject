#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
using namespace std;
class drawn {
	public:
		string drawBody[13] = {" |              _____",
							   " |            /      \\",
							   " |            |      |",
							   " |            |      |",
							   " |            \\______/",
							   " |               |    ",
							   " |              /|\\  ",
							   " |             / | \\ ",
							   " |               |    ",
							   " |               |    ",
							   " |              / \\  ",
							   " |             /   \\ ",
							   " |                 "
							   };
		int h1 = 12;	
};
class characterCheck {
	public:
		void usrEnterChar(char &usrchar)
		{
			cout << "Enter a Single Character: ";
			cin >> usrchar;
		}
		void checkWordEqual(char &usrchar, string &word2, string &word3, bool &start, int &NumberTries)
		{
			char* copyStr = new char[word2.length() + 1];
			strcpy(copyStr, word3.c_str());
			if(strchr(copyStr, usrchar) != NULL) {
				for(int i = 0; i < word3.length(); i++)
				{
					if(word3[i] == usrchar)
					{
						word2[i] = usrchar;
						word2[0] = toupper(word2[0]);
					}
				}
			} else {
				NumberTries++;
				if(NumberTries == 7) {
					start = false;
					word3[0] = toupper(word3[0]);
					cout << "\t" << word3 << endl;
					cout << "\tGameOver" << endl;
				}  
				//Make add +1 to count a drawing to complete hangman drawing
			}
			delete[] copyStr;
		}
		void stopGame(char &usrchar, string &word2, string &word3, bool &start)
		{
			char* arrayW = new char[word2.length() + 1];
			strcpy(arrayW, word2.c_str()); 
			if(strchr(arrayW, '_') != NULL)
			{
				start = true;
			} else {
				start = false;
				cout << "\tYou Win" << endl;
			}
			delete[] arrayW;
		}
};
void draw(int NumberTries, drawn bodyCreated, drawn s1[]);
int main(void)
{
	srand(time(NULL));
	int randomC = 0;
	bool start = true;
	int NumberTries = 0;
	drawn bodyCreated;
	drawn s1[12];
	//USING FILE I/O to add words in this game
	string words[5] = {"Wave", "Revolutionary", "Jump", "Assault", "Statement"};
	randomC = rand() % 5;
	string word2 = words[randomC];
	string word3 = words[randomC]; 
	char usrchar;
	characterCheck player;
	drawn NumTry;
	for(int x = 0; x < word3.length(); x++){word3[x] = tolower(word3[x]);}
	for(int d = 0; d < word2.length(); d++) {word2[d] = '_';}
	while(start)
	{
		system("cls");
		draw(NumberTries, bodyCreated, s1);
		cout << "\t" << word2 << endl;
		player.stopGame(usrchar, word2, word3, start);
        if(start == false) {break;}
		player.usrEnterChar(usrchar);
		player.checkWordEqual(usrchar, word2, word3, start, NumberTries);
	}
	return 0;
}
void draw(int NumberTries, drawn bodyCreated, drawn s1[])
{
	switch(NumberTries)
	{
		case 1:
			s1[0].h1=0;
			s1[1].h1=1;
			break;
		case 2:
			s1[2].h1=2;
			s1[3].h1=3;
			break;
		case 3:
			s1[4].h1=4;
			s1[5].h1=5;
			break;
		case 4:
			s1[6].h1=6;
			s1[7].h1=7;
			break;
		case 5:
			s1[8].h1=8;
			s1[9].h1=9;
			break;
		case 6:
			s1[10].h1=10;
			s1[11].h1=11;
			break;
	}
	cout << "-------------------" << endl;
	cout << " |                |" << endl;
	cout << " |                |" << endl;
	cout << " |                |" << endl;
	cout << bodyCreated.drawBody[s1[0].h1] << endl; //1
	cout << bodyCreated.drawBody[s1[1].h1] << endl; //2
	cout << bodyCreated.drawBody[s1[2].h1] << endl;//3
	cout << bodyCreated.drawBody[s1[3].h1] << endl;//4
	cout << bodyCreated.drawBody[s1[4].h1] << endl;//5
	cout << bodyCreated.drawBody[s1[5].h1] << endl;//6
	cout << bodyCreated.drawBody[s1[6].h1] << endl;//7
	cout << bodyCreated.drawBody[s1[7].h1] << endl;//8
	cout << bodyCreated.drawBody[s1[8].h1] << endl;//9
	cout << bodyCreated.drawBody[s1[9].h1] << endl;//10
	cout << bodyCreated.drawBody[s1[10].h1] << endl;//11
	cout << bodyCreated.drawBody[s1[11].h1] << endl;//12
	cout << " |                 " << endl;//13
	cout << "/-\\______" << endl;
	cout << endl;
}